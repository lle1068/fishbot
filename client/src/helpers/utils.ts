export const timer = (ms: number): Promise<void> =>
  new Promise((res) => setTimeout(res, ms));

export const scrollToBottom = (id: string): NodeJS.Timeout => {
  return setTimeout(() => {
    const element = document.getElementById(id);

    if (element) {
      element.scrollTop = element.scrollHeight;
    }
  }, 200);
};

export const scrollIntoView = (id: string): NodeJS.Timeout => {
  return setTimeout(() => {
    const element = document.getElementById(id);
    element?.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
      inline: 'nearest',
    });
  }, 200);
};
