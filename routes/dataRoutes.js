const fetch = require('node-fetch');

module.exports = (app) => {
  app.get('/api/data/:fish_name', async (req, res, next) => {
    const fishName = req.params.fish_name;

    if (!fishName) {
      return res.status(400).text('Missing fish name.');
    }

    try {
      const data = await fetch(
        `https://www.fishwatch.gov/api/species/${fishName}`
      ).then((response) => response.json());

      res.status(200).json(data);
    } catch (error) {
      console.error(error);
      next(error);
    }
  });
};
